import { createGlobalStyle } from 'styled-components';

export const IconfontStyle = createGlobalStyle`
@font-face {
  font-family: "iconfont";
  src: url("iconfont.eot?t=1609038961360"); /* IE9 */
  src: url("iconfont.eot?t=1609038961360#iefix") format("embedded-opentype"),
    /* IE6-IE8 */
      url("data:application/x-font-woff2;charset=utf-8;base64,d09GMgABAAAAAAPEAAsAAAAAB+wAAAN3AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHEIGVgCDFAqDOIMRATYCJAMQCwoABCAFhG0HQBvzBhHVmzHJfhy4sdOw3vWQkIvkFumfg4DqrGokO9cTLaSz9EoO8B/ES1YCYHMz7mWNilAsyXneT9+Rjk3Ir/+5nDbF57ezXG5NaqxBY9lRL8A4oADHWhRIgRZIgt4wvWpS8Ohvaz8EYEiiAFG7buPWWGjwMAGIwQP69cBqWdAzpQILgXNkrkYswIElZ8qzwHz1ffGGksICiUOBxzbvW6c31Z9HPC+r/EF/Z6GAPM8FhItAAQWABtF91NMRZTIFKEzeXVMUcKIRyLNCwaBSlSEKzkn+8kAiUCCc4GIXAKVRVsHzCIZAwvMyCAQ8L1uEAvgZgy3AAFOAg8iaAH5njUSHGeOKcllWpN2u3drofhsDeW03zMrpvSSmtX/poLWzM6us9Mf0btu63fLoNksiWs9aNsQza4l/qe2dvTywbELfozfE9ettr15tfe1am9k3qvVZOntjYOKNG+2a2MALXL/uNW1drc0gu0PHZUlmTUz7Hgft2WCEOWZmnReBdu0CZlblNrvW4V9vzz4pRKD8drv6+5cvRSHjx/sKMee+12aHc9yfCd7C5NofCz8tXDB6VImVur170MgRex+2cwzXnejx1vUh+oNLjKVS9xCPp0vOsGpy8cmTixVBIcbXXXTy1GJZbXjuNY9nZVypuDKPvqspW7dOAayCbjlF1phE4dtwrG+9Szm1L/XxBFWt6wOrNmsja/YcUPWG/lN11OjT5YsJR42iw3LbDnB3sRtVr/i0a7vRZ2Ij/8RlfspMYjgAwZdqkYpR+uPqmKqhDNIO+RtKdwr72c1b+ZtlawDuehJHwHDwuGZ9ARYSQtLZyJUNuqCxVeuMYUTp1ZRs9EMCBgM4VzlAGPfPkCJXF4PAIrIGSQiJoLDIUGptATiwKQ1OLKqBIZ96i23CGYlC6CJAHlMBCHxsBYmHo6DwcU6ptffAQRTvwIlPKDC0FeFr2mQJeICoRSgZDag/BN2qpXYWuvSG7p0lNWmleyGdIQ7LOHfzV6xIZ0w4P25ltmCpFdjBYZhzg04toubRM/dtmuzYk0bdyiCoRSgZDag/BN2qFa1Z9D5/Q/fOkmZkc/MX0hk6h2WcVyivQl0l+5ZHzo9bmS1YagV24MJMMg36eK+Imke/I9W3CTWza4Xxfql84TbAgNfUQgoltHCge4YYRF9ZnZEaFelLDTkMAA==")
      format("woff2"),
    url("iconfont.woff?t=1609038961360") format("woff"), url("./iconfont.ttf?t=1609038961360") format("truetype"),
    url("iconfont.svg?t=1609038961360#iconfont") format("svg");
}

.iconfont {
  font-family: "iconfont" !important;
  font-size: 16px;
  font-style: normal;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
`
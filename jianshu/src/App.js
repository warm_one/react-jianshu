import React, { Fragment } from 'react';
import { GlobalStyle } from './style';
import { IconfontStyle } from './statics/iconfont/iconfont'
import Header from './common/header/index'
import store from './store/index'
import { Provider } from 'react-redux'

function App() {
  return (
    <Fragment>
      <GlobalStyle />
      <IconfontStyle />
      <Provider store={store}>
        <Header />
      </Provider>
    </Fragment>
  );
}

export default App;

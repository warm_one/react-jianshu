import React, { Component, Fragment } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import './style.css'
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show: true,
      list: []
    }
    this.handleToggole = this.handleToggole.bind(this)
    this.handleAddItem = this.handleAddItem.bind(this)
  }
  render() {
    return (
      <Fragment>
        {/* <CSSTransition
          in={this.state.show}
          timeout={300}
          classNames='fade'
          unmountOnExit
          onEntered={(el) => { el.style.color = "blue" }}
          appear={true}
        >
          <div>{6666666}</div>
        </CSSTransition> */}
        {/* <div className={this.state.show ? 'show' : 'hide'}>hello Mike NB!</div> */}
        {/* <button onClick={this.handleToggole}>toggole</button> */}
        <TransitionGroup>
          {
            this.state.list.map((item, index) => {
              return (
                <CSSTransition
                  timeout={300}
                  classNames='fade'
                  unmountOnExit
                  onEntered={(el) => { el.style.color = "blue" }}
                  appear={true}
                  key={index}
                >
                  <div >{item}</div>
                </CSSTransition>
              )
            })
          }
        </TransitionGroup>
        <div>hello!! NB</div>
        <button onClick={this.handleAddItem}>Toggle</button>
      </Fragment>
    )
  }
  handleAddItem() {
    this.setState((prevState) => {
      return {
        list: [...prevState.list, 'item']
      }
    })
  }
  handleToggole() {
    this.setState(() => ({
      show: this.state.show ? false : true
    }))
  }
}
export default App;


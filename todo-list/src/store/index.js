import { createStore,applyMiddleware,compose } from 'redux'
import thunk from 'redux-thunk';
import reducer from './reducer'
// , window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) :compose
const enhancer = composeEnhancers(
  applyMiddleware(thunk)
)
const store = createStore(reducer, enhancer)


export default store

// store是唯一的
// 只有store能改变store的内容
// Reducer是纯函数  给定固定的输入，就一定会有固定的输出，而且不会由任何副作用（参数的修改）


// 核心API
// createStore
// store.dispatch
// store.getState
// store.subscribe
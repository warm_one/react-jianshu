const Mock = require('mockjs');

const url = {
  tableDataOne: 'http://localhost:3000/test/json'
}
module.exports = [
  Mock.mock(url.tableDataOne, {
    'string': ['MIKE', 'JEKK', 'JEENY']
  })
]
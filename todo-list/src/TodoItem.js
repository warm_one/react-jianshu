import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TodoItem extends Component {
  constructor(props) {
    super(props)
    this.handClick = this.handClick.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.content !== this.props.content) {
      return true
    } else {
      return false
    }
  }

  render() {
    // 当父组件的render函数被运行时，它的子组件的render都将被重新运行
    const { content, test } = this.props
    return (
      <div onClick={this.handClick}>{test}-{content}</div>
    )
  }

  handClick() {
    const { deletItem, index } = this.props
    deletItem(index)
    // this.props.deletItem(this.props.index)
  }
}

TodoItem.propTypes = {
  test: PropTypes.string.isRequired,
  content: PropTypes.string,
  deletItem: PropTypes.func,
  index: PropTypes.number
}

TodoItem.defaultProps = {
  test: 'hello world'
}

export default TodoItem
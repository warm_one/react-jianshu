import React, { Component } from 'react'
import 'antd/dist/antd.css';
import store from './store/index'
import TodoListAntUi from './TodoListAntUi'
import axios from 'axios'
import './mock/api'
import { getInputChangeAction, getAddItemAction, deleteTodoItem, initListAction } from './store/actionCreators'
// import { CHANGE_INPUT_VALUE, ADD_TODO_ITEM, DELETE_TODO_ITEM } from './store/actionType'

class TodoListAnt extends Component {
  constructor(props) {
    super(props)
    this.state = store.getState()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleStoreChange = this.handleStoreChange.bind(this)
    this.handleBtnClick = this.handleBtnClick.bind(this)
    this.handleItemClick = this.handleItemClick.bind(this)
    store.subscribe(this.handleStoreChange)
  }

  render() {
    return (<TodoListAntUi
      inputValue={this.state.inputValue}
      handleInputChange={this.handleInputChange}
      handleBtnClick={this.handleBtnClick}
      list={this.state.list}
      handleItemClick={this.handleItemClick}
    />)
  }

  componentDidMount() {
    axios.get('http://localhost:3000/test/json').then((res) => {

      const data = res.data.string
      console.log(data)
      const action = initListAction(data)
      store.dispatch(action)
    })
  }

  handleInputChange(e) {
    // const action = {
    //   type: CHANGE_INPUT_VALUE,
    //   value: e.target.value
    // }
    const action = getInputChangeAction(e.target.value)
    store.dispatch(action)
  }
  handleStoreChange() {
    this.setState(store.getState())
  }
  handleBtnClick() {
    // const action = {
    //   type: ADD_TODO_ITEM
    // }
    const action = getAddItemAction()
    store.dispatch(action)
  }
  handleItemClick(index) {
    // const action = {
    //   type: DELETE_TODO_ITEM,
    //   index
    // }
    const action = deleteTodoItem(index)
    store.dispatch(action)
  }
}

export default TodoListAnt
import React from 'react'
import { Input, Button, List } from 'antd';

// 当一个组件中只有render函数时，可以更改成无状态组件
// 优势： 性能比较高
const TodoListAntUi = (props) => {
  return (
    <div style={{ marginTop: '10px', marginLeft: '10px' }}>
      <div>
        <Input
          value={props.inputValue}
          placeholder="todo info"
          style={{ width: '300px', marginRight: '10px' }}
          onChange={props.handleInputChange}
        ></Input>
        <Button type="primary" onClick={props.handleBtnClick}>提交</Button>
      </div>
      <List
        style={{ marginTop: '10px', maxWidth: "300px" }}
        bordered
        dataSource={props.list}
        renderItem={(item) => (<List.Item onClick={(index) => { props.handleItemClick(index) }}>{item}</List.Item>)}
      />
    </div>
  )
}

// class TodoListAntUi extends Component {
//   render() {
//     return (
//       <div style={{ marginTop: '10px', marginLeft: '10px' }}>
//         <div>
//           <Input
//             value={this.props.inputValue}
//             placeholder="todo info"
//             style={{ width: '300px', marginRight: '10px' }}
//             onChange={this.props.handleInputChange}
//           ></Input>
//           <Button type="primary" onClick={this.props.handleBtnClick}>提交</Button>
//         </div>
//         <List
//           style={{ marginTop: '10px', maxWidth: "300px" }}
//           bordered
//           dataSource={this.props.list}
//           renderItem={(item) => (<List.Item onClick={(index) => { this.props.handleItemClick(index) }}>{item}</List.Item>)}
//         />
//       </div>
//     )
//   }
// }

export default TodoListAntUi
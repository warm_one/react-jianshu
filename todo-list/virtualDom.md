1. state数据
2. JSX模板
3. 数据 + 模板 生成虚拟DOM（虚拟DOM是一个JS对象，用它来描述真实DOM）（损耗了性能）
  ['div',{id:'abc}],['span': {}, 'hello world']
4. 用虚拟DOM的结构生成真实的DOM，来显示
  <div id="abc"><span>hello world</span></div>
5. state发生变化
6. 数据 + 模板 生成新的虚拟DOM（极大的提升了性能） 
   ['div',{id:'abc}],['span',{}, 'bye bye']
7. 比较原始虚拟DOM和新的虚拟DOM的区别，找到区别是span中内容（极大的提升性能）
8. 直接操作DOM，改变span中的内容


优点：
1. 性能提升了
2. 它使得跨端应用得以实现。 React Native

###### 生命周期
initalization :setup props and state
Mounting: componmentWillMount->render->componentDidMount
Updation: 
        props: componentWillReceiveProps -> shouldComponentUpdate ->render -> componentDidUpdate
        state: shouldComponentUpdate -> componentWillUpdate->render->componentDidUpdate
Unmounting: componentWillUnmount